
-- Use script: sudo mysql < fill.sql;

-- DATABASE:    polart
-- TYPE ENUM:   
-- TABLE:       
--              
-- -------------------------------------------------------------
-- unix> sudo mysql
-- mysql> ...
-- show databases;
-- use polart;
-- show tables;
-- describe table...;
-- select * from table...;
-- quit;
-- exit;
-- -------------------------------------------------------------

USE polart;

-- TABLE Produit  -----------------------------

INSERT INTO evenement (nom, date_evt, id_cat, img_princ)
    VALUES 
            -- 17 Productions
            ( "Production 01", NULL, 4, "prod01ajardin.jpeg"),
            ( "Production 02", NULL, 4, "prod02acuis.jpeg"),
            ( "Production 03", "2017-06-01", 4, "prod03courcopie1.jpeg"),
            ( "Production 04", NULL, 4, "prod03courcopie2.jpeg"),
            ( "Production 05", NULL, 4, "prod03courcopie3.jpeg"),
            ( "Production 06", NULL, 4, "prod03courcopie4.jpeg"),
            ( "Production 07", NULL, 4, "prod03courcopie5.jpeg"),
            ( "Production 08", "2019-06-01", 4, "prod03courcopie6.jpeg"),
            ( "Production 09", NULL, 4, "prod03courcopie7.jpeg"),
            ( "Production 10", NULL, 4, "prod03courcopie8.jpeg"),
            ( "Production 11", NULL, 4, "prod03courcopie9.jpeg"),
            ( "Production 12", NULL, 4, "prod03courcopie10.jpeg"),
            ( "Production 13", NULL, 4, "prod03courcopie11.jpeg"),
            ( "Production 14", NULL, 4, "prod03courcopie12.jpeg"),
            ( "Production 15", NULL, 4, "prod03courcopie13.jpeg"),
            ( "Production 16", NULL, 4, "prod03courcopie14.jpeg"),
            ( "Production 17", NULL, 4, "prod03courcopie15.jpeg"),

            ("Cuisine 01", NULL, 3, "cuisine1.jpeg"),
            ("Cuisine 02", NULL, 3, "cuisine2.jpeg"),
            ("Cuisine 03", NULL, 3, "cuisine3.jpeg"),
            ("Cuisine 04", "2019-06-02", 3, "cuisine4.jpeg"),
            ("Cuisine 05", NULL, 3, "cuisine5.jpeg"),
            ("Cuisine 06", NULL, 3, "cuisine6.jpeg"),
            ("Cuisine 07", NULL, 3, "cuisine7.jpeg"),

            ("Cour 01", NULL, 2, "cours1.jpeg"),
            ("Cour 02", NULL, 2, "cours2.jpeg"),
            ("Cour 03", "2019-11-22", 2, "cours3.jpeg"),
            ("Cour 04", NULL, 2, "cours4.jpeg"),
            ("Cour 05", NULL, 2, "cours5.jpeg"),
            
            ("Jardin 01", "2019-09-09", 1, "jardin1.jpeg"),
            ("Jardin 02", NULL, 1, "jardin2.jpeg"),
            ("Jardin 03", NULL, 1, "jardin3.jpeg"),
            ("Jardin 04", NULL, 1, "jardin4.jpeg");


INSERT INTO images (fichier)
    VALUES 
            ( "prod01ajardin.jpeg"),
            ( "prod01bjardin.jpeg"),
            ( "prod01cjardin.jpeg"),

            ( "prod02acuis.jpeg"),
            ( "prod02bcuis.jpeg"),
            ( "prod02ccuis.jpeg"),

            ( "prod03courcopie1.jpeg"),
            ( "prod03courcopie2.jpeg"),
            ( "prod03courcopie3.jpeg"),
            ( "prod03courcopie4.jpeg"),
            ( "prod03courcopie5.jpeg"),
            ( "prod03courcopie6.jpeg"),
            ( "prod03courcopie7.jpeg"),
            ( "prod03courcopie8.jpeg"),
            ( "prod03courcopie9.jpeg"),
            ( "prod03courcopie10.jpeg"),
            ( "prod03courcopie11.jpeg"),
            ( "prod03courcopie12.jpeg"),
            ( "prod03courcopie13.jpeg"),
            ( "prod03courcopie14.jpeg"),
            ( "prod03courcopie15.jpeg"),

            ( "cuisine1.jpeg"),              -- Evt Cuisine 01 : 6 images
            ( "cuisine2.jpeg"),
            ( "cuisine3.jpeg"),
            ( "cuisine4.jpeg"),
            ( "cuisine5.jpeg"),
            ( "cuisine6.jpeg"),

            ( "cuisine2.jpeg"),              -- Evt Cuisine 02
            ( "cuisine3.jpeg"),              -- Evt Cuisine 03
            ( "cuisine4.jpeg"),              -- Evt Cuisine 04
            ( "cuisine5.jpeg"),              -- Evt Cuisine 05
            ( "cuisine6.jpeg"),              -- Evt Cuisine 06
            ( "cuisine8.jpeg"),              -- Evt Cuisine 07

            ( "cours1.jpeg"),          -- Evt Cour 1 a 5
            ( "cours2.jpeg"),
            ( "cours3.jpeg"),  
            ( "cours4.jpeg"),  

            ( "cours2.jpeg"),          -- Evt Cour 1 a 5
            ( "cours3.jpeg"),          -- Evt Cour 1 a 5
            ( "cours4.jpeg"),          -- Evt Cour 1 a 5
            ( "cours5.jpeg"),          -- Evt Cour 1 a 5

            ( "jardin1.jpeg"),          -- Evt Jardin 1 4 images
            ( "jardin2.jpeg"),
            ( "jardin3.jpeg"),
            ( "jardin4.jpeg"),

            ( "jardin2.jpeg"),          -- Evt Jardin 2 3 images
            ( "jardin3.jpeg"),               
            ( "jardin4.jpeg");              


INSERT INTO evenement_images (id_evenement, id_image, ordre)
    VALUES  
            (1, 1, 1),  -- "Production 01"          -- 3 images
            (1, 2, 2),
            (1, 3, 3),
            (2, 4, 1),  -- "Production 02"          -- 3 images
            (2, 5, 2),
            (2, 6, 3),
            (3, 7, 1),  -- "Production 03"          -- 1 image
            (4, 8, 1),  -- "Production 04 etc..."   -- 1 image
            (5, 9, 1),
            (6, 10, 1),
            (7, 11, 1),
            (8, 12, 1),
            (9, 13, 1),
            (10, 14, 1),
            (11, 15, 1),
            (12, 16, 1),
            (13, 17, 1),
            (14, 18, 1),
            (15, 19, 1),
            (16, 20, 1),
            (17, 21, 1),

            -- Evt Cuisine 01 : 6 images
            (18, 22, 1),
            (18, 23, 2),
            (18, 24, 3),
            (18, 25, 4),
            (18, 26, 5),
            (18, 27, 6),

            -- Evt Cuisine 02 a 7 : 1 mages
            (19, 28, 1),
            (20, 29, 1),
            (21, 30, 1),
            (22, 31, 1),
            (23, 32, 1),
            (24, 33, 1),

            -- Evt cours 01 : 4 images
            (25, 34, 1),
            (25, 35, 2),
            (25, 36, 3),
            (25, 37, 4),
            
            -- Evt Cour 2 a 5: / 1 image chacun
            (26, 38, 1),
            (27, 39, 1),
            (28, 40, 1),
            (29, 41, 1),

            -- evt Jardin 1 : 4 images
            (30, 42, 1),
            (30, 43, 2),
            (30, 44, 3),
            (30, 45, 4),

            -- Evt Jardin 1 a 2 : / 1 image chacun
            (31, 46, 1),
            (32, 47, 1),
            (33, 48, 1);

INSERT INTO content (id, titre, soustitre, texte)
    VALUES 
            (1, "Titre1", "SousTitre1", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (2, "Titre2", "SousTitre2", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (3, "Titre3", "SousTitre3", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (4, "Titre4", "SousTitre4", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (5, "Titre5", "SousTitre5", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (6, "Titre6", "SousTitre6", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (7, "Titre7", "SousTitre7", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (8, "Titre8", "SousTitre8", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (9, "Titre9", "SousTitre9", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (10, "Titre10", "SousTitre10", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (11, "Titre11", "SousTitre11", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (12, "Titre12", "SousTitre12", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (13, "Titre13", "SousTitre13", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (14, "Titre14", "SousTitre14", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (15, "Titre15", "SousTitre15", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (16, "Titre16", "SousTitre16", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (17, "Titre17", "SousTitre17", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (18, "Titre18", "SousTitre18", "Phasellus sed dolor nec sem pharetra blandit. Donec commodo est nulla, sit amet ornare nulla ultrices non. Nullam rhoncus dolor eget tortor accumsan ultrices. Etiam laoreet augue eu ex pellentesque, a facilisis nibh volutpat. Sed volutpat velit urna, vel fringilla orci sagittis eu. Etiam consectetur, enim eu scelerisque efficitur, libero justo faucibus dolor, sed ornare nisi eros id turpis. Pellentesque pulvinar tempus tortor non posuere. Ut vitae bibendum nisl, id dapibus velit. Cras sem tortor, imperdiet non tincidunt vel, porttitor ut metus." ),
            (19, "Titre19", "SousTitre19", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (20, "Titre20", "SousTitre20", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (21, "Titre21", "SousTitre21", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (22, "Titre22", "SousTitre22", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (23, "Titre23", "SousTitre23", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (24, "Titre24", "SousTitre24", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (25, "Titre25", "SousTitre25", "Donec ullamcorper et nunc nec lacinia. Suspendisse fringilla nisl vitae risus efficitur, dictum convallis mauris sollicitudin. Nullam leo lorem, finibus sit amet sapien quis, egestas dictum enim. Morbi fringilla ipsum vel ipsum imperdiet pharetra. Praesent commodo velit et leo finibus, fringilla malesuada turpis sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque suscipit accumsan mauris, ac pharetra dui aliquam sit amet. Pellentesque at venenatis felis, et imperdiet lorem. In eget lorem quis nisi hendrerit varius eget rutrum lacus. Phasellus rutrum auctor metus." ),
            (26, "Titre26", "SousTitre26", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (27, "Titre27", "SousTitre27", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (28, "Titre28", "SousTitre28", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (29, "Titre29", "SousTitre29", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (30, "Titre30", "SousTitre30", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (31, "Titre31", "SousTitre31", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (32, "Titre32", "SousTitre32", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            (33, "Titre33", "SousTitre33", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." );


INSERT INTO evenement_content (id_evenement, id_content, ordre)
        VALUES
            -- Production 1 à 17
            (1, 1, 1),         
            (2, 2, 1),          
            (3, 3, 1),          
            (4, 4, 1), 
            (5, 5, 1),
            (6, 6, 1),
            (7, 7, 1),
            (8, 8, 1),
            (9, 9, 1),
            (10, 10, 1),
            (11, 11, 1),
            (12, 12, 1),
            (13, 13, 1),
            (14, 14, 1),
            (15, 15, 1),
            (16, 16, 1),
            (17, 17, 1),

            -- Evt Cuisine 1 a 7
            (18, 18, 1),
            (19, 19, 1),
            (20, 20, 1),
            (21, 21, 1),
            (22, 22, 1),
            (23, 23, 1),
            (24, 24, 1),

            -- Evt Cour 1 a 5 
            (25, 25, 1),
            (26, 26, 1),
            (27, 27, 1),
            (28, 28, 1),
            (29, 29, 1),

            -- Evt Jardin 1 a 2 
            (30, 30, 1),
            (31, 31, 1),
            (32, 32, 1),
            (33, 33, 1);
