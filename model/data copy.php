<?php 

include 'debug.php';

$host = 'localhost';
$db   = 'polart';
$user = 'admin';
$pass = 'admin';

// CONNEXION BDD =====================================================

try {
    $pdo = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo "Erreur : " . $e->getMessage();
}


// CRUD : Create ===================================================
// insérer ici les function de création / insertion...


// CRUD : Read =====================================================

function getInfo() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM info;");
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

function getArtiste() {
    global $pdo;
    $req = $pdo->query('SELECT nom, prenom, url FROM artiste;');
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

function getMOP() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM cat WHERE genre = 'mop';");
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

function getMopEvents() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM evenement WHERE ;");
}

function getProd() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM cat WHERE genre = 'prod';");
    return $req->fetchAll();
}

function getEvts($idCat) {
    global $pdo;
    $sql = "SELECT * FROM evenement WHERE id_cat = ?";
    $req = $pdo->prepare($sql);
    $req->execute([$idCat]);
    return $req->fetchAll();

    // global $pdo;
    // $stm = $pdo->prepare('SELECT * FROM produit WHERE id = ?;');
    // $stm->execute([$id]);
    // return $stm->fetch();
}

function getEvtId($id) {
    global $pdo;
    $sql = "SELECT * FROM evenement WHERE id = ?";
    $req = $pdo->prepare($sql);
    $req->execute([$id]);
    return $req->fetchAll();
}

function getEvtsN1N2($idCat, $num1, $nb ) {
    global $pdo;

    // $sql = "SELECT * FROM evenement WHERE id_cat = '$idCat' ORDER BY 'id' ASC LIMIT $num1, $nb ";
    $sql = "SELECT * FROM evenement WHERE id_cat = :idCat ORDER BY id ASC LIMIT :num1 , :nb";
    echo "</p> sql 2 = $sql";
    
    $req = $pdo->prepare($sql);
    echo "</p> var_dump(req)  = ";
    var_dump($req);

    $req->bindValue(':idCat', $idCat, PDO::PARAM_INT);
    $req->bindValue(':num1', $num1, PDO::PARAM_INT);
    $req->bindValue(':nb', $nb, PDO::PARAM_INT);
    
    // $stm = $req->execute( [$idCat]);
    $stm = $req->execute( );
    echo "</p> var_dump(stm)  = ";
    var_dump($stm);

    return $req->fetchAll();
}


function getEvtImages($id) {
    global $pdo;
    $sql = "SELECT fichier, ordre FROM images INNER JOIN evenement_images
            ON images.id = evenement_images.id_image
            WHERE id_evenement = ?
            ORDER BY ordre ASC";
    echo "</p> sql = $sql";
    $req = $pdo->prepare($sql);
    $req->execute([$id]);
    return $req->fetchAll();
}

// faire une fonction qui prends en parametre un nom/id, et qui renvoie toutes
// les informations relatives au projet, images, textBlock, titre, ordre


// CRUD : Update =====================================================

// CRUD : Delete =====================================================


?>