DROP DATABASE IF EXISTS polart;
CREATE DATABASE polart;
USE polart;

GRANT ALL PRIVILEGES ON polart.* TO 'admin'@'localhost';
flush privileges;

CREATE TABLE info (
    section1 VARCHAR(50),
    section2 VARCHAR(50),
    section3 VARCHAR(50),
    section4 VARCHAR(50),
    url_adhesion VARCHAR(250),
    description_asso TEXT
);

CREATE TABLE administrateur (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(50),
    mdp VARCHAR(100)
);

CREATE TABLE cat (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(250),
    genre ENUM("mop", "prod"),
    img VARCHAR(255),
    soustitre VARCHAR(255),
    text_description TEXT
);

CREATE TABLE evenement (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(100),
    id_cat INT UNSIGNED,
    date_evt DATE,
    lieu VARCHAR(100),
    img_princ VARCHAR(50),
    lien VARCHAR(255),
    FOREIGN KEY (id_cat) REFERENCES cat(id)
);

CREATE TABLE cat_evenement (
    id_evenement INT UNSIGNED,
    id_cat INT UNSIGNED,
    UNIQUE (id_evenement, id_cat),
    FOREIGN KEY (id_evenement) REFERENCES evenement(id),
    FOREIGN KEY (id_cat) REFERENCES cat(id)
);

CREATE TABLE images (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    fichier VARCHAR(100)
);

CREATE TABLE evenement_images (
    id_evenement INT UNSIGNED,
    id_image INT UNSIGNED,
    ordre INT UNSIGNED,
    UNIQUE (id_evenement, id_image),
    FOREIGN KEY (id_evenement) REFERENCES evenement(id),
    FOREIGN KEY (id_image) REFERENCES images(id)
);

CREATE TABLE content (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(100),
    soustitre VARCHAR(100),
    texte TEXT
);

CREATE TABLE evenement_content (
    id_evenement INT UNSIGNED,
    id_content INT UNSIGNED,
    ordre INT UNSIGNED,
    UNIQUE (id_evenement, id_content),
    FOREIGN KEY (id_evenement) REFERENCES evenement(id),
    FOREIGN KEY (id_content) REFERENCES content(id)
);

CREATE TABLE artiste (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(30),
    prenom VARCHAR(30),
    url VARCHAR(255)
);

-- INSERT INTO TABLE ---------------------------------
INSERT INTO cat (nom, genre, img, soustitre, text_description) 
    VALUES 
        ('Côté jardin', 'mop', "assets/jardin1.jpeg", "sous titre cuisine", "description cuisine"), 
        ('Côté cours', 'mop', "assets/cours1.jpeg","sous titre jardin", "description cuisine"),
        ('Côté cuisine', 'mop', "assets/cuisine1.jpeg", "sous titre cour", "description cuisine"),
        ('Productions', NULL,   NULL                    ,  NULL              , NULL          );
        
INSERT INTO info (section1, section2, section3, section4, url_adhesion, description_asso) 
    VALUES (
        'intentions',
        'menu orchestré participatif',
        'productions',
        'adhésion',
        'https://www.helloasso.com/associations/lepol-art-association/adhesions/bulletin-d-adhesion-2021-lepol-art-association-2/widget-bouton',
        "LEPOL’art forme à la fois un lieu de création et de recherche dédié aux artistes visuels, et un outil de médiation auprès de tous. Nous proposons ainsi stages, ateliers et réalisations d’actions artistiques créés selon et pour un contexte spécifique, en collaboration directe.
LEPOL’art est aussi un support pour expérimenter les processus proposés en entreprise. Cette phase de l’expérience permet aussi de questionner en ouvrant les champs d’activités en réinventant ensemble et en partage. Ce stimulus créatif donne naissance à la tentative comme outil d’équilibre.
L’invitation de  plusieurs artistes, penseurs, professionnels et bénévoles qui, ensemble, prennent part aux différentes propositions de la programmation avec conviction. Ils forment un groupe solidaire et libre ensemble."
);

INSERT INTO artiste (nom, prenom, url)
    VALUES 
        ('artiste1', 'test', 'https://fr.wikipedia.org/wiki/Cheval'),
        ('artiste2', 'test', 'https://fr.wikipedia.org/wiki/Cheval'),
        ('artiste3', 'test', 'https://fr.wikipedia.org/wiki/Cheval'),
        ('artiste5', 'test', 'https://fr.wikipedia.org/wiki/Cheval'),
        ('artiste6', 'test', 'https://fr.wikipedia.org/wiki/Cheval'),
        ('artiste7', 'test', 'https://fr.wikipedia.org/wiki/Cheval');

