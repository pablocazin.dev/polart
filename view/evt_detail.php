<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation des menus Orchestre participatif">
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick-theme.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <title>Evénements</title>
</head>

<body>
    <div class="header">
        <?php 
        include '../model/data.php';
        $onglets  = getInfo()[0];
        $idEvt = $_GET['page'];
        include 'header.php'?>
    </div>
    <main class="al-center">

        <?php 
        // $mopCat = getEvts($id_cat);
        // $idEvt = $mopCat[0]["id"];

        $evtSelected = getEvtId($idEvt); 
        $imagesOrdre = getEvtImages($idEvt);
        $contentsOrdre = getEvtContents($idEvt);
        $nbImg = count($imagesOrdre);
        if( $nbImg > 8 ) {
            $nbImg = 8;
        }
        $date = " ";
        if( $evtSelected["date_evt"] > 0) {
            $date = "le ".$evtSelected["date_evt"]." ";
        }
        $lieu = " ";
        if( $evtSelected["lieu"] > 0) {
            $lieu = "à ".$evtSelected["lieu"]." ";
        }
        ?>

        <div class="detail-barre"></div>
        <div class="ligne axe1-sp-around axe2-center">
            <h2 ><?php  echo $evtSelected["nom"]; ?></h2>
            <h3 ><?php  echo $date; ?></h3>
            <h3 ><?php  echo $lieu; ?></h3>
        </div>
        <div class="detail-barre"></div>
        
        <div class="wrapper-pictxt">

            <div class="box-text-left">
                <p class="title-mop"><?php echo $evtSelected["nom"]; ?></p>
                <p class="sub-title-mop"><?php echo $contentsOrdre[0]["soustitre"]; ?></p>
                <div class="box-paragraphe">
                    <p><?php echo $contentsOrdre[0]["texte"]; ?></p>
                </div>
                <div class="box-ancre">
                    <a class="ancre-inscr" href="">Inscription</a>
                </div>
            </div>

            <div class="box-picture">
                <div class="firstpic">
                    <img id="first-pic" src="<?php echo "./assets/".$imagesOrdre[0]["fichier"] ?>" alt="">
                </div>

                <?php for($i=1; $i<$nbImg; $i++) { ?> 
                    <img class="others-pic" src="<?php echo "./assets/".$imagesOrdre[$i]["fichier"] ?>" alt="">
                <?php } ?>
            </div>

            
        </div>


    </main>
    <div class="footer">
        <?php 
    include 'footer.php'
    ?>
    </div>

    <script type=" text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js">
    </script>
    <script type="text/javascript" src="../control/mopapp.js"></script>
</body>

</html>