<?php $onglets  = getInfo()[0] ?>

<header>
    <div class="headtop">
        <div class="logo"><a href="./index.php"><img src="./assets/logo 120x120.gif"></img></a></div>
        <div class="titre">
            <h1>LEPOL'ART</h1>
            <span>Association sous loi 1901</span>
        </div>
        <div class="social">
            <a href="https://www.instagram.com/?hl=fr"><img src="./assets/insta.png" alt="logoInstagram" width="70px"
                    height="70px"></a>
            <a href="https://fr.linkedin.com/"><img src="./assets/linke.png" alt="logoLinkedin" width="65px"
                    height="65px"></a>
            <a href="https://fr-fr.facebook.com/"><img src="./assets/faceb.png" alt="logoFacebook" width="70px"
                    height="70px"></a>
        </div>
    </div>
    <div id="headbarre"></div>
    <div class="headbottom">
        <nav>
            <a href="./index.php">
                <h3><?php echo $onglets["section1"]; ?></h3>
            </a>
            <a href="./mop.php">
                <h3><?php echo $onglets["section2"]; ?></h3>
            </a>
            <a href="./productions.php">
                <h3><?php echo $onglets["section3"]; ?></h3>
            </a>
            <a href="<?php echo $onglets["url_adhesion"]; ?>">
                <h3><?php echo $onglets["section4"]; ?></h3>
            </a>
        </nav>
    </div>
</header>