<?php include '../model/data.php';
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style/style.css">
    <title>LEPOL'ART</title>
</head>

<body class="col-center">
    <?php 
    include 'header.php'
    ?>
    <main>

        <?php $intervenant = json_encode(getArtiste()); ?>

        <div id="animBanBox" data-inter="">
            <img id="imgBan" src="./assets/BAN1170x400.jpg" alt="bannière">
        </div>

        <p id="paraph">"Amener de la poésie et du jeu dans un environnement de travail qui, a priori, en est dénué en
                    transformant un quotidien morose en un rituel festif et joyeux."</p>

        <div id="eventBox">
            <div class="splitBox">
                <div class="splitBarre"></div>
                <h3>Notre champ d’actions</h3>
            </div>

            <?php $actu = getActu(); ?>

            <div class="eventDetails" id="event1">
                <h4><?php echo $actu[0]["date_evt"]?></h4>
                <div class="bulle"><img id="1" src="<?php echo "./assets/".$actu[0]["img_princ"]?>"></div>
                <div class="eventTxtDiv">
                    <p><?php echo $actu[0]["nom"]?></p>
                    <a href="./evt_detail.php?page=<?= $actu[0]["id"] ?>">Lire la suite...</a>
                </div>
            </div>
            <div class="eventDetails" id="event2">
                <h4><?php echo $actu[1]["date_evt"]?></h4>
                <div class="bulle"><img id="2" src="<?php echo "./assets/".$actu[1]["img_princ"]?>"></div>
                <div class="eventTxtDiv">
                    <p><?php echo $actu[1]["nom"]?></p>
                    <a href="./evt_detail.php?page=<?= $actu[1]["id"] ?>">Lire la suite...</a>
                </div>
            </div>
            <div class="eventDetails" id="event3">
                <h4><?php echo $actu[2]["date_evt"]?></h4>
                <div class="bulle"><img id="3" src="<?php echo "./assets/".$actu[2]["img_princ"]?>"></div>
                <div class="eventTxtDiv">
                    <p><?php echo $actu[2]["nom"]?></p>
                    <a href="./evt_detail.php?page=<?= $actu[2]["id"] ?>">Lire la suite...</a>
                </div>
            </div>
            <div class="eventDetails" id="event4">
                <h4><?php echo $actu[3]["date_evt"]?></h4>
                <div class="bulle"><img id="4" src="<?php echo "./assets/".$actu[3]["img_princ"]?>"></div>
                <div class="eventTxtDiv">
                    <p><?php echo $actu[3]["nom"]?></p>
                    <a href="./evt_detail.php?page=<?= $actu[3]["id"] ?>">Lire la suite...</a>
                </div>
            </div>
            <div class="eventDetails" id="event5">
                <h4><?php echo $actu[4]["date_evt"]?></h4>
                <div class="bulle"><img id="5" src="<?php echo "./assets/".$actu[4]["img_princ"]?>"></div>
                <div class="eventTxtDiv">
                    <p><?php echo $actu[4]["nom"]?></p>
                    <a href="./evt_detail.php?page=<?= $actu[4]["id"] ?>">Lire la suite...</a>
                </div>
            </div>

            <div class="splitBox">
                <div class="splitBarre"></div>
                <h3>Qui Sommes-Nous ?</h3>
            </div>


            <div class="descAsso">
                <div id="btnAsso">&#9759;</div>
                <p><?php echo $onglets["description_asso"]?></p>
            </div>

        </div>
    </main>
    <?php 
    include 'footer.php'
    ?>
</body>

<script>
const myTags = <?php echo $intervenant; ?>
</script>
<script src="https://cdn.jsdelivr.net/npm/TagCloud@2.2.0/dist/TagCloud.min.js"></script>
<script src="../control/intentions.js"></script>

</html>