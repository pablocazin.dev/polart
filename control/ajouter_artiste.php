<?php 

include '../model/data.php';

if(
    isset($_POST["nom"]) && $_POST["nom"] !== "" &&
    isset($_POST["prenom"]) && $_POST["prenom"] !== "" &&
    isset($_POST["url"]) && $_POST["url"] !== "" 
) {

$nom = $_POST["nom"];
$prenom = $_POST["prenom"];
$url = $_POST["url"];

addArtist($nom, $prenom, $url);
}
header('Location: ../view/admin/admin.php'); 
?>

