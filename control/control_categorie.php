<?php 

include '../model/data.php';

if(
    isset($_POST["cat1"]) && $_POST["cat1"] !== "" &&
    isset($_POST["cat2"]) && $_POST["cat2"] !== "" &&
    isset($_POST["cat3"]) && $_POST["cat3"] !== "" &&
    isset($_POST["url1"]) && $_POST["url1"] !== "" &&
    isset($_POST["url2"]) && $_POST["url2"] !== "" &&
    isset($_POST["url3"]) && $_POST["url3"] !== "" &&
    isset($_POST["titre1"]) && $_POST["text1"] !== "" &&
    isset($_POST["titre2"]) && $_POST["text2"] !== "" &&
    isset($_POST["titre3"]) && $_POST["text3"] !== "" 
) {
    $array = array (
        array (
            'cat' => $_POST["cat1"],
            'url' => $_POST["url1"],
            'titre' => $_POST["titre1"],
            'text' => $_POST["text1"]
        ),
        array (
            'cat' => $_POST["cat2"],
            'url' => $_POST["url2"],
            'titre' => $_POST["titre2"],
            'text' => $_POST["text2"]
        ),
        array (
            'cat' => $_POST["cat3"],
            'url' => $_POST["url3"],
            'titre' => $_POST["titre3"],
            'text' => $_POST["text3"]
        )
    );
    updateCat($array);
}
header('Location: ../view/admin/admin.php'); 
?>

